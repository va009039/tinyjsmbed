#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
from google.appengine.ext.webapp import util
from google.appengine.ext.webapp import template
import logging
import os
import string
import md5

target_info = {
'lpc1768': {
    'bin_name': "main_LPC1768.bin",
    'tinyjs_bin': "TinyJS_LPC1768.bin",
    'usr_pos': 69632,
    },
'lpc4088': {
    'bin_name': "main_LPC4088.bin",
    'tinyjs_bin': "TinyJS_LPC4088.bin",
    'usr_pos': 102400,
    },
'k64f' : {
    'bin_name': "main_K64F.bin",
    'tinyjs_bin': "TinyJS_K64F.bin",
    'usr_pos': 106496,
    },
'f401re': {
    'bin_name': "main_NUCLEO_F401RE.bin",
    'tinyjs_bin': "tinyJS_NUCLEO_F401RE.bin",
    'usr_pos': 69632,
    },
'f411re': {
    'bin_name': "main_NUCLEO_F411RE.bin",
    'tinyjs_bin': "tinyJS_NUCLEO_F411RE.bin",
    'usr_pos': 69632,
    },
'l152re': {
    'bin_name': "main_NUCLEO_L152RE.bin",
    'tinyjs_bin': "tinyJS_NUCLEO_L152RE.bin",
    'usr_pos': 69632,
    },
};

class MainHandler(webapp2.RequestHandler):
    def build(self, source, filename = "main.js", target="lpc1768"):
        bin_name = target_info[target]['bin_name']
        tinyjs_bin = target_info[target]['tinyjs_bin']
        usr_pos = target_info[target]['usr_pos']
        bin = open(tinyjs_bin, "rb").read()
        pad_size = usr_pos - len(bin)
        if pad_size < 0:
            logging.error("pad_size: %d", pad)
            self.html_output("error-03")
        logging.info("src size: %d bytes, src-md5: %s", len(source), md5.new(source).hexdigest())
        new_bin = bin + '+'*pad_size + str(source) + chr(0)
        logging.info("bin size: %d bytes, bin-md5: %s", len(new_bin), md5.new(new_bin).hexdigest())
        self.response.headers['Content-Type'] = "application/octet-stream"
        self.response.headers['Content-Disposition'] = 'attachment; filename="%s"' % bin_name
        self.response.out.write(new_bin)

    def export(self, source, filename = "main.js"):
        self.response.headers['Content-Type'] = "text/plain"
        self.response.headers['Content-Disposition'] = 'attachment; filename="%s"' % filename
        self.response.out.write(source)

    def html_output(self, msg=""):
        template_values = {'msg':msg}
        path = os.path.join(os.path.dirname(__file__), 'tinyjsmbed.html')
        self.response.out.write(template.render(path, template_values))

    def post(self):
        source = self.request.get('source')
        source = source.replace("\r", "");
        mode = self.request.get('mode')
        if mode == 'export':
            self.export(source)
            return
        target = self.request.get('target')
        self.build(source, target=target)

    def get(self):
        self.html_output()

app = webapp2.WSGIApplication([
    ('/.*', MainHandler)
], debug=True)
